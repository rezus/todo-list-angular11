const express = require('express');

const app = express();

app.use(express.static('./dist/todo-list-angular11'));

app.get('/*', (req, res) =>
    res.sendFile('index.html', {root: 'dist/todo-list-angular11/'}),
);

app.listen(process.env.PORT || 8080);
